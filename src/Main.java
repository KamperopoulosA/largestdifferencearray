public class Main {
    public static void main(String[] args) {

        int[] arr = {2,3,1,7,9,5,11,3,5};
        int largest_diff = 0;
        for(int i=0; i< arr.length; i++){

            for(int j=0; j<arr.length; j++)
            if(arr[j] - arr[i] > largest_diff){
                largest_diff = arr[j] - arr[i];
            }

        }
        System.out.println("Largest diff is: "+largest_diff);
        int count =0;
        for(int i=0; i< arr.length; i++){
            for(int j=i+1; j<arr.length; j++){
                if(arr[i]+arr[j]==largest_diff){
                    count++;
                }
            }
        }
        System.out.println("There are "+ count +" that add up to maximum "+largest_diff);

    }
}